import { useEffect, useState } from 'react';
import './App.css';
import DateList from './components/dateList';
import Character from './components/character';
import SearchBar from './components/searchBar';
import queries from './config/queries';
import decodeData from './tools/queries-decoder';

const App = () => {
  const [events] = useState([]);
  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    fetch(`https://query.wikidata.org/sparql?query=${queries.politicianLiveAndDeath}&format=json`)
      .then((response) => response.json())
      .then((response) => console.log(response));
  }, []);

  const submitSearch = (e) => {
    e.preventDefault();
  };

  return (
    <div className="App">
      <SearchBar
        setSearchValue={setSearchValue}
        submitSearch={(e) => submitSearch(e, searchValue)}
      />
      <DateList searchValue />
      <ul>
        {
          events.map((event, index) => <><Character key={`event-${index}`} data={event} /><br /></>)
        }
      </ul>
    </div>
  );
};

export default App;
