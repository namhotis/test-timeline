const SearchBar = ({ submitSearch, setSearchValue, searchValue }) => (
  <form onSubmit={(e) => submitSearch(e, searchValue)}>
    <input
      type="text"
      value={searchValue}
      onChange={(e) => setSearchValue(e.target.value)}
    />
    <input type="submit" />
  </form>
);

export default SearchBar;
