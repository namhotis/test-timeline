const CharacterInfo = ({ dataKey, dataValue }) => (dataKey && dataValue ? `${dataKey} ${dataValue}` : null);

const Character = ({ data }) => {
  if (typeof data[0][0] === 'string') { // If not a NodeList
    return data.map((info, index) => (
      <>
        <CharacterInfo key={`info-${index}`} dataKey={info[0]} dataValue={info[1]} />
        <br />
      </>
    ));
  }

  return null;
};

export default Character;
