const politicianLiveAndDeath = encodeURIComponent(`SELECT distinct ?politicianLabel ?politician ?datebirth ?datedeath ?placebirthLabel (CONCAT(STR(?lat),", ",STR(?long)) as ?lat_long) ?placedeathLabel (CONCAT(STR(?lat_2),", ", STR(?long_2)) as ?lat_long_2)

WHERE {
?politician wdt:P106 wd:Q82955 ;
            wdt:P27 wd:Q142;
          p:P570/psv:P570 [
            wikibase:timeValue ?date;
            wikibase:timePrecision ?precision
          ].
  FILTER("1890-01-01"^^xsd:dateTime < ?date && ?date < "1899-12-31"^^xsd:dateTime).
  FILTER(?precision >= 9).
  BIND(YEAR(?date) AS ?year).
  ?politician wdt:P19 ?placebirth. # lieu de naissance 
  ?politician wdt:P569 ?datebirth.
  ?politician wdt:P570 ?datedeath. 
  ?placebirth wdt:P625 ?coord.
  ?placebirth p:P625 ?declaration.
  ?declaration psv:P625 ?coord_geo.
  ?coord_geo wikibase:geoLatitude ?lat.
  ?coord_geo wikibase:geoLongitude ?long.
  ?politician wdt:P20? ?placedeath. 
  ?placedeath wdt:P625 ?coord_death.
  ?placedeath p:P625 ?declaration2.
  ?declaration2 psv:P625 ?coord_geo_2.
  ?coord_geo_2 wikibase:geoLatitude ?lat_2.
  ?coord_geo_2 wikibase:geoLongitude ?long_2.
  SERVICE wikibase:label { 
  bd:serviceParam wikibase:language "fr,en"}
}

LIMIT 30`);

const wikidataItemsOfWikipediaArticles = () => `
  SELECT ?lemma ?item WHERE {
  VALUES ?lemma {
    "Wikipedia"@de
    "Wikidata"@de
    "Berlin"@de
    "Technische Universität Berlin"@de
  }
  ?sitelink schema:about ?item;
    schema:isPartOf <https://de.wikipedia.org/>;
    schema:name ?lemma.
}`;

const queries = {
  politicianLiveAndDeath,
  wikidataItemsOfWikipediaArticles,
};

export default queries;
